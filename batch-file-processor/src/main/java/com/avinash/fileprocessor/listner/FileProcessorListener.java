package com.avinash.fileprocessor.listner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class FileProcessorListener {

    private static Logger logger = LoggerFactory.getLogger(FileProcessorListener.class);

    @Value("${input.file.location}")
    private String inputLocation;

    @Value("${input.file.process}")
    private String processLocation;

    @Value("${input.file.archive}")
    private String archiveLocation;

    private Path inputSource;

    @Scheduled(fixedDelay=5000)
    public void process() {
        logger.info("Scheduler started at {} ", System.nanoTime());
        inputSource = Paths.get(inputLocation);
        moveFilesForProcessing(inputSource);
    }

    private void moveFilesForProcessing(Path inputSource) {
        List<Path> files = new ArrayList<>();
        try (Stream<Path> walker = Files.walk(inputSource, 1)) {
            walker.forEach((path) -> {
                if (Files.isRegularFile(path)) {
                    files.add(path);
                }
           });
        } catch (IOException e) {
            logger.warn("Failed on walking remote directory > " + e.getMessage(), e);
        }
        for (Path file : files) {
            String fileName = file.getFileName().toString();
        }
    }
}
